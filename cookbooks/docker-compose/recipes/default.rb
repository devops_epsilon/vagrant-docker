execute "install docker-compose" do
  command "curl -L https://github.com/docker/compose/releases/download/1.5.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose"
  user "root"
  not_if { ::File.exists?("/usr/local/bin/docker-compose") }
end
