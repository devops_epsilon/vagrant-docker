execute "Add the new gpg key" do
  command "apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D"
end

execute "Add repository" do
  command "echo deb https://apt.dockerproject.org/repo ubuntu-trusty main > /etc/apt/sources.list.d/docker.list"
end

execute "Update the apt package index" do
  command "apt-get update"
end

execute "Purge the old repo if it exists" do
  command "apt-get purge lxc-docker*"
end

package "docker-engine"

group "docker" do
  members "vagrant"
  append true
end

service "docker" do
  provider Chef::Provider::Service::Upstart
  action :restart
end
